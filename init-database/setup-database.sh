#!/bin/bash

# 1. Create and start the PostgreSQL container
docker-compose -f ./init-database/create-database.docker-compose.yml up -d

# Wait for PostgreSQL to be ready (you may need to adjust the sleep duration)
sleep 3

# 2. Execute the SQL initialization script inside the container
docker exec -i init-database-zerfickungsbot-db-1 psql -U zerfickungsbot -d zerfickungsbot_local < ./init-database/init-database.sql

sleep 3

docker-compose -f ./init-database/create-database.docker-compose.yml down

echo "Script executed successfully!"
