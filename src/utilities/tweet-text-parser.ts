import * as emojiFlags from 'emoji-flags';
import * as lookup from 'country-code-lookup';

import { Fixture } from '../types/fixture.type';

function getFlagEmoji(countryName: string): string {
  const flag = lookup.byCountry(countryName) as lookup.CountryCode | null;
  if (flag) {
    return emojiFlags.countryCode(flag.iso2).emoji;
  } else {
    switch (countryName) {
      case 'Hong-Kong':
        return emojiFlags.countryCode('HK').emoji;
      case 'South-Korea':
        return emojiFlags.countryCode('KR').emoji;
      case 'England':
        return emojiFlags.countryCode('GB').emoji;
      case 'Wales':
        return emojiFlags.countryCode('GB').emoji;
      case 'Czech-Republic':
        return emojiFlags.countryCode('CZ').emoji;
      case 'Scotland':
        return emojiFlags.countryCode('GB').emoji;
      case 'Saudi-Arabia':
        return emojiFlags.countryCode('SA').emoji;
      case 'Northern-Ireland':
        return emojiFlags.countryCode('GB').emoji;
      case 'Congo-DR':
        return emojiFlags.countryCode('CD').emoji;
      case 'Costa-Rica':
        return emojiFlags.countryCode('CR').emoji;
      case 'United-Arab-Emirates':
        return emojiFlags.countryCode('AE').emoji;
      case 'New-Zealand':
        return emojiFlags.countryCode('NZ').emoji;
      case 'Faroe-Islands':
        return emojiFlags.countryCode('FO').emoji;
      case 'Macedonia':
        return emojiFlags.countryCode('MK').emoji;
      case 'San-Marino':
        return emojiFlags.countryCode('SM').emoji;
      case 'Palestine':
        return emojiFlags.countryCode('PS').emoji;
      case 'Bosnia':
        return emojiFlags.countryCode('BA').emoji;
      case 'USA':
        return emojiFlags.countryCode('US').emoji;
      case 'World':
        return '🌍';
      default:
        return '🚩';
    }
  }
}

export function createTweetText(fixture: Fixture): string {
  return `📢 Zerfickungsalarm 📢\n⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯\n${getFlagEmoji(
    fixture.league.country
  )} ${fixture.league.country} (${fixture.league.name})\n\n⚽ ${
    fixture.homeTeam.team_name
  } ${fixture.goalsHomeTeam}:${fixture.goalsAwayTeam} ${fixture.awayTeam.team_name}`;
}
