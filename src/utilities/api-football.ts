import axios, { AxiosRequestConfig } from 'axios';
import {
  fixtureEnded,
  fixtureIsNotYouthGame,
  fixtureIsZerfickung
} from './custom-array-filter';

import { Fixture } from '../types/fixture.type';

interface AllFixturesApiResponse {
  data: { api: { fixtures: Fixture[] } };
}

function getRapidApiKey(): string {
  const key =
    process.env.NODE_ENV === 'production' ? process.argv[6] : process.env.RAPID_API_KEY;
  if (key) {
    return key;
  } else {
    throw new Error('rapid-api-key is not provided');
  }
}

function getAllGamesRequestConfig(date: Date): AxiosRequestConfig {
  const requestDate = date.toISOString().split('T')[0];
  const requestConfig: AxiosRequestConfig = {
    method: 'GET',
    url: `https://api-football-v1.p.rapidapi.com/v2/fixtures/date/${requestDate}`,
    params: { timezone: 'Europe/London' },
    headers: {
      'x-rapidapi-key': getRapidApiKey(),
      'x-rapidapi-host': 'api-football-v1.p.rapidapi.com'
    }
  };

  return requestConfig;
}

export async function getPostableFixtures(): Promise<Fixture[]> {
  const date = new Date();
  const requestConfig = getAllGamesRequestConfig(date);
  const response: AllFixturesApiResponse = await axios.request(requestConfig);
  return response.data.api.fixtures
    .filter(fixtureEnded)
    .filter(fixtureIsZerfickung)
    .filter(fixtureIsNotYouthGame);
}
