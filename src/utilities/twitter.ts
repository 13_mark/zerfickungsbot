import { TweetV2PostTweetResult, TwitterApi } from 'twitter-api-v2';

interface TwitterApiCredentials {
  appKey: string;
  appSecret: string;
  accessToken: string;
  accessSecret: string;
}

function getTwitterApiCredentials(): TwitterApiCredentials {
  const config = {
    appKey:
      process.env.NODE_ENV === 'production' ? process.argv[2] : process.env.CONSUMER_KEY,
    appSecret:
      process.env.NODE_ENV === 'production'
        ? process.argv[3]
        : process.env.CONSUMER_SECRET,
    accessToken:
      process.env.NODE_ENV === 'production' ? process.argv[4] : process.env.ACCESS_TOKEN,
    accessSecret:
      process.env.NODE_ENV === 'production'
        ? process.argv[5]
        : process.env.ACCESS_TOKEN_SECRET
  };
  if (
    !!config.appKey &&
    !!config.appSecret &&
    !!config.accessToken &&
    !!config.accessSecret
  ) {
    return config as TwitterApiCredentials;
  }
  throw new Error('Twitter API-Credentials are not provided in the correct way.');
}

const twitter = new TwitterApi(getTwitterApiCredentials());
const client = twitter.readWrite.v2;

export function sendTweet(text: string): Promise<TweetV2PostTweetResult> {
  return client.tweet(text);
}
