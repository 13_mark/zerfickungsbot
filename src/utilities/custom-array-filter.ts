// import * as fs from 'fs';

import { Fixture } from '../types/fixture.type';

export const fixtureEnded = (fixture: Fixture): boolean => {
  return fixture.statusShort === 'FT';
};

export const fixtureIsZerfickung = (fixture: Fixture): boolean => {
  return (
    fixture.goalsHomeTeam - fixture.goalsAwayTeam >= 6 ||
    fixture.goalsAwayTeam - fixture.goalsHomeTeam >= 6
  );
};

export const fixtureIsNotYouthGame = (fixture: Fixture): boolean => {
  return !areBothTeamsYouthTeams(fixture);
};

// export const fixtureWasNotPostedEarlier = (fixture: Fixture): boolean => {
//   return !fixtureWasPostedEarlier(fixture);
// };

function areBothTeamsYouthTeams(fixture: Fixture): boolean {
  const youthTeamSearchStrings = ['U17', 'U18', 'U19', 'U20', 'U21', 'U22', 'U23'];
  for (const outerSearchString of youthTeamSearchStrings) {
    for (const innerSearchString of youthTeamSearchStrings) {
      if (
        fixture.homeTeam.team_name.includes(outerSearchString) &&
        fixture.awayTeam.team_name.includes(innerSearchString)
      ) {
        return true;
      }
    }
  }
  return false;
}

// const fixtureWasPostedEarlier = (fixture: Fixture): boolean => {
//   const allEarlierPostedMatches: Pick<
//     Fixture,
//     'homeTeam' | 'awayTeam' | 'event_timestamp'
//   >[] = JSON.parse(fs.readFileSync('posted_games.json', 'utf-8'));
//   return allEarlierPostedMatches.some((earlierFixture) => {
//     return (
//       earlierFixture.homeTeam.team_name === fixture.homeTeam.team_name &&
//       earlierFixture.awayTeam.team_name === fixture.awayTeam.team_name &&
//       earlierFixture.event_timestamp === fixture.event_timestamp
//     );
//   });
// };
