import { Client } from 'pg';

function generatePostgresConnectionString() {
  const user =
    process.env.NODE_ENV === 'production' ? process.argv[7] : process.env.DATABASE_USER;
  const password =
    process.env.NODE_ENV === 'production'
      ? process.argv[8]
      : process.env.DATABASE_PASSWORD;
  const host =
    process.env.NODE_ENV === 'production' ? process.argv[9] : process.env.DATABASE_HOST;
  const port =
    process.env.NODE_ENV === 'production' ? process.argv[10] : process.env.DATABASE_PORT;
  const database =
    process.env.NODE_ENV === 'production'
      ? process.argv[11]
      : process.env.DATABASE_DATABASE;
  return `postgresql://${user}:${password}@${host}:${port}/${database}`;
}

export class PostgresHandler {
  async executeQuery(queryString: string, args?: any[]) {
    const client = new Client({
      connectionString: generatePostgresConnectionString()
    });
    await client.connect();

    const result = await client.query(queryString, args);

    await client.end();

    return result;
  }
}
