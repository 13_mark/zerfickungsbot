import { Fixture } from '../types/fixture.type';
import { PostgresHandler } from './postgres-handler';

interface FixtureWithGeneratedId {
  fixture: Fixture;
  databaseId: string;
}

export async function addNewFixturesToDatabase(
  postableFixtures: readonly Fixture[]
): Promise<readonly Fixture[]> {
  const postgresHandler = new PostgresHandler();

  const fixturesWithFixtureIds: readonly FixtureWithGeneratedId[] = postableFixtures.map(
    (fixture) => ({ fixture, databaseId: createFixtureId(fixture) })
  );
  const notPostedFixtures = await filterAlreadyPostedFixtures(
    fixturesWithFixtureIds,
    postgresHandler
  );

  await writeNotPostedFixtures(notPostedFixtures, postgresHandler);

  return notPostedFixtures.map(({ fixture }) => fixture);
}

function createFixtureId({ homeTeam, awayTeam, event_timestamp }: Fixture) {
  return `${event_timestamp}-${homeTeam.team_name}-${awayTeam.team_name}`;
}

async function isFixtureAlreadyPosted(
  fixtureId: string,
  postgresHandler: PostgresHandler
): Promise<boolean> {
  const res = await postgresHandler.executeQuery(
    `SELECT * from zerfickungsbot_persistance Where id = $1`,
    [fixtureId]
  );
  return Promise.resolve(!!res.rowCount && res.rowCount > 0);
}

async function filterAlreadyPostedFixtures(
  values: readonly FixtureWithGeneratedId[],
  postgresHandler: PostgresHandler
) {
  const promises = values.map(({ fixture, databaseId }) =>
    isFixtureAlreadyPosted(databaseId, postgresHandler).then((isAlreadyPosted) => ({
      isAlreadyPosted,
      fixture,
      databaseId
    }))
  );

  const result = await Promise.all(promises);
  return result.filter(({ isAlreadyPosted }) => !isAlreadyPosted);
}

async function writeNotPostedFixtures(
  ids: readonly { databaseId: string }[],
  postgresHandler: PostgresHandler
) {
  for (const { databaseId } of ids) {
    await postgresHandler.executeQuery(
      `INSERT INTO zerfickungsbot_persistance (id) VALUES ($1)`,
      [databaseId]
    );
  }
}
