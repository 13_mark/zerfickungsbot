import { Fixture } from './fixture.type';

export interface FixtureWithTweetText {
  fixture: Fixture;
  tweetText: string;
}
