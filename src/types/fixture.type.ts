export interface Fixture {
  homeTeam: Team;
  awayTeam: Team;
  event_timestamp: number;
  statusShort: 'FT' | 'NS' | 'HT';
  league: League;
  goalsHomeTeam: number;
  goalsAwayTeam: number;
}

interface League {
  name: string;
  country: string;
  flag: string;
}

interface Team {
  team_name: string;
}
