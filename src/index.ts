import { Fixture } from './types/fixture.type';
import { addNewFixturesToDatabase } from './utilities/add-new-fixtures-to-database';
import { getPostableFixtures } from './utilities/api-football';
import { createTweetText } from './utilities/tweet-text-parser';
import { sendTweet } from './utilities/twitter';

async function main() {
  const postableFixtures: readonly Fixture[] = await getPostableFixtures();

  const notAlreadyPostedFixtures = await addNewFixturesToDatabase(postableFixtures);

  const fixturesWithTweetText = notAlreadyPostedFixtures.map((fixture) => ({
    fixture,
    tweetText: createTweetText(fixture)
  }));

  fixturesWithTweetText.forEach((fixtureWithTweetText) => {
    sendTweet(fixtureWithTweetText.tweetText).catch((err) =>
      console.log(`An error occured - Code ${err.code}`)
    );
  });
}

main();
