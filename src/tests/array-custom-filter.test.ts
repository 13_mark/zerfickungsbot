import {
  fixtureEnded,
  fixtureIsNotYouthGame,
  fixtureIsZerfickung
} from '../utilities/custom-array-filter';

import { Fixture } from '../types/fixture.type';

const testGameEndedFixture: Omit<Fixture, 'statusShort'> = {
  awayTeam: {
    team_name: 'team1'
  },
  homeTeam: {
    team_name: 'team2'
  },
  event_timestamp: 213,
  goalsAwayTeam: 2,
  goalsHomeTeam: 3,
  league: {
    country: 'testCountry',
    flag: 'HK',
    name: 'testName'
  }
};

const testZerfickungenFixture: Omit<Fixture, 'goalsHomeTeam' | 'goalsAwayTeam'> = {
  awayTeam: {
    team_name: 'team1'
  },
  event_timestamp: 213,
  homeTeam: {
    team_name: 'team2'
  },
  league: {
    country: 'testCountry',
    flag: 'JK',
    name: 'testName'
  },
  statusShort: 'FT'
};

const testYouthFixutre: Omit<Fixture, 'awayTeam' | 'homeTeam'> = {
  event_timestamp: 213,
  goalsHomeTeam: 2,
  goalsAwayTeam: 4,
  league: {
    country: 'Test',
    flag: 'test',
    name: 'test'
  },
  statusShort: 'FT'
};

test('filters ended fixtures', () => {
  const input: Fixture[] = [
    {
      ...testGameEndedFixture,
      statusShort: 'FT'
    },
    {
      ...testGameEndedFixture,
      statusShort: 'HT'
    },
    {
      ...testGameEndedFixture,
      statusShort: 'NS'
    },
    {
      ...testGameEndedFixture,
      statusShort: 'FT'
    },
    {
      ...testGameEndedFixture,
      statusShort: 'HT'
    }
  ];
  expect(input.filter(fixtureEnded)).toEqual([input[0], input[3]]);
});

test('filters zerfickungen', () => {
  const input: Fixture[] = [
    {
      ...testZerfickungenFixture,
      goalsAwayTeam: 9,
      goalsHomeTeam: 2
    },
    {
      ...testZerfickungenFixture,
      goalsAwayTeam: 9,
      goalsHomeTeam: 7
    },
    {
      ...testZerfickungenFixture,
      goalsAwayTeam: 0,
      goalsHomeTeam: 5
    },
    {
      ...testZerfickungenFixture,
      goalsAwayTeam: 1,
      goalsHomeTeam: 9
    },
    {
      ...testZerfickungenFixture,
      goalsAwayTeam: 2,
      goalsHomeTeam: 2
    }
  ];

  expect(input.filter(fixtureIsZerfickung)).toEqual([input[0], input[3]]);
});

test('filters youth Fixtures', () => {
  const input: Fixture[] = [
    {
      ...testYouthFixutre,
      awayTeam: {
        team_name: 'testTeam U21'
      },
      homeTeam: {
        team_name: 'testTeam U21'
      }
    },
    {
      ...testYouthFixutre,
      awayTeam: {
        team_name: 'testTeam U20'
      },
      homeTeam: {
        team_name: 'testTeam'
      }
    },
    {
      ...testYouthFixutre,
      awayTeam: {
        team_name: 'testTeam'
      },
      homeTeam: {
        team_name: 'testTeam'
      }
    },
    {
      ...testYouthFixutre,
      awayTeam: {
        team_name: 'testTeam U18'
      },
      homeTeam: {
        team_name: 'testTeam U18'
      }
    },
    {
      ...testYouthFixutre,
      awayTeam: {
        team_name: 'testTeam'
      },
      homeTeam: {
        team_name: 'testTeam U21'
      }
    }
  ];

  expect(input.filter(fixtureIsNotYouthGame)).toEqual([input[1], input[2], input[4]]);
});

test('filters youth Fixtures where the youth teams differ', () => {
  const input: Fixture[] = [
    {
      ...testYouthFixutre,
      awayTeam: {
        team_name: 'testTeam U21'
      },
      homeTeam: {
        team_name: 'testTeam U21'
      }
    },
    {
      ...testYouthFixutre,
      awayTeam: {
        team_name: 'testTeam'
      },
      homeTeam: {
        team_name: 'testTeam'
      }
    },
    {
      ...testYouthFixutre,
      awayTeam: {
        team_name: 'testTeam U18'
      },
      homeTeam: {
        team_name: 'testTeam U21'
      }
    }
  ];

  expect(input.filter(fixtureIsNotYouthGame)).toEqual([input[1]]);
});
