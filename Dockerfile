FROM node:18-bullseye

ENV CONSUMER_KEY=
ENV CONSUMER_SECRET=
ENV ACCESS_TOKEN=
ENV ACCESS_TOKEN_SECRET=
ENV RAPID_API_KEY=

ENV DATABASE_USER=
ENV DATABASE_PASSWORD=
ENV DATABASE_HOST=
ENV DATABASE_PORT=
ENV DATABASE_DATABASE=

WORKDIR /usr/zerfickungsalarm
COPY package.json /usr/zerfickungsalarm/

RUN npm install
COPY ./ /usr/zerfickungsalarm

RUN npm run build
COPY ./ /usr/zerfickungsalarm

CMD ["sh", "-c", "NODE_ENV=production node dist/index.js $CONSUMER_KEY $CONSUMER_SECRET $ACCESS_TOKEN $ACCESS_TOKEN_SECRET $RAPID_API_KEY $DATABASE_USER $DATABASE_PASSWORD $DATABASE_HOST $DATABASE_PORT $DATABASE_DATABASE"]